To deploy the add-ins to Visual Studio, please follow these steps:
1) Build the project
2) Copy the Assembly DLL file (for example HeloWorld.dll) from Bin folder and the .AddIn XML file (for example HelloWorld.AddIn) from root folder to 
[Install Drive]:\Users\[local user]\Documents\Visual Studio 2010\Addins\ in Visual Studio 2010 and 
[Install Drive]:\Users\[local user]\Documents\Visual Studio 2012\Addins\ in Visual Studio 2012

If you follow the steps as explained in the book i.e., by using wizard to create the project, VisualStudio will create a testing add-in and will place that 
in the Documents\VisualStudio\Addins folder for testing purpose.